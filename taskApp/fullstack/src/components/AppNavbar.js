import { useState, useContext } from 'react';
import UserContext from '../UserContext';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import React from 'react';
import { Navbar, Button } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import '../App.css';

const AppNavbar = () => {
  const { user } = useContext(UserContext);


  return (
    <Navbar className="navbarStyle" expand="lg">
      <Navbar.Brand className="mx-2 text-light logo-text">Task Management</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" className="mx-2 navbar-toggler" />
      <Navbar.Collapse className="justify-content-end">
        <Nav className="ms-auto">
          {user.id !== null ? (
            <Nav.Link className="mx-2 text-light" as={Link} to="/logout">LOGOUT</Nav.Link>
          ) : (
            <Nav.Link className="mx-2 text-light" as={Link} to="/login">LOGIN</Nav.Link>
          )}
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
};

export default AppNavbar;
