import React, { useState, useEffect } from 'react';
import { Modal, Form, Button } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import '../App.css';

const AddTaskComponent = () => {
  const [showModal, setShowModal] = useState(false);
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [priority, setPriority] = useState('Low');
  const [dueDate, setDueDate] = useState('');
  const [tasks, setTasks] = useState([]);
  const navigate = useNavigate();

  const handleShowModal = () => setShowModal(true);
  const handleCloseModal = () => {
    setShowModal(false);
    // Optional: Reset the form fields when the modal is closed
    setTitle('');
    setDescription('');
    setPriority('Low');
    setDueDate('');
  };

  const token = localStorage.getItem('token');

  const handleAddTask = () => {

    const newTask = {
      title,
      description,
      dueDate,
      priority
    };

    fetch('https://letigio-taskapp.onrender.com/task/add-task', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
      body: JSON.stringify(newTask),
    })
      .then((response) => response.json())
      .then((data) => {
        if (data === true) {
          // Task added successfully
          console.log('Task added!');
          console.log(data);
          handleCloseModal();

          // After the task is added, fetch the updated list of tasks
          fetchTasks();
          // navigate('/task',{replace: true});
        } else {
          console.log('Task not added!');
        }
      })
      .catch((error) => console.error('Error adding task:', error));
  };


  useEffect(() => {
    // Fetch tasks initially
    fetchTasks();
  }, []);


  const fetchTasks = () => {
    // Fetch tasks
    fetch('https://letigio-taskapp.onrender.com/task/allTask', {
      headers: {
        'Authorization': `Bearer ${token}`
      },
    })
      .then((response) => response.json())
      .then((data) => {
        setTasks(data); // Update tasks with the fetched data, including the newly added task
      })
      .catch((error) => console.error('Error fetching tasks:', error));
  };

  return (
    <div>
      <Button className="login-bottom my-2" onClick={handleShowModal}>
        Add Task
      </Button>

      <Modal show={showModal} onHide={handleCloseModal}>
        <Modal.Header closeButton>
          <Modal.Title>Adding Task</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={handleAddTask}>
            <Form.Group>
              <Form.Label>Title</Form.Label>
              <Form.Control
                type="text"
                value={title}
                onChange={(e) => setTitle(e.target.value)}
              />
            </Form.Group>

            <Form.Group>
              <Form.Label>Description</Form.Label>
              <Form.Control
                as="textarea"
                rows={3}
                value={description}
                onChange={(e) => setDescription(e.target.value)}
              />
            </Form.Group>

            <Form.Group>
              <Form.Label>Priority</Form.Label>
              <Form.Control
                as="select"
                value={priority}
                onChange={(e) => setPriority(e.target.value)}
              >
                <option>Low</option>
                <option>Medium</option>
                <option>High</option>
              </Form.Control>
            </Form.Group>

            <Form.Group>
              <Form.Label>Due Date</Form.Label>
              <Form.Control
                type="date"
                value={dueDate}
                onChange={(e) => setDueDate(e.target.value)}
              />
            </Form.Group>

            <Button type="submit" className="login-bottom my-3">
              Save Task
            </Button>
          </Form>
        </Modal.Body>
      </Modal>
    </div>
  );
};

export default AddTaskComponent;
