import React, { useEffect, useState, useContext } from 'react';
import UserContext from '../UserContext';

const UserDetails = () => {
  const {user} = useContext(UserContext);
  const [userData, setUserData] = useState({});

  useEffect(() => {
    fetch('https://letigio-taskapp.onrender.com/users/details', {
      headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            }
    })
    .then(res => res.json())
    .then(data => {
            console.log(data)
            // Set the user states values with the user details upon successful login.
            if (typeof data._id !== "undefined") {

                setUserData(data);

            }
        });
  }, []);

  return (
    <div>
      <p>Name: {userData.name}</p>
      <p>Email: {userData.email}</p>
    </div>
  );
};

export default UserDetails;
