import React, { useState, useEffect } from 'react';
import Swal from 'sweetalert2';
import { useParams } from 'react-router-dom';
import '../App.css';
import { Button, Table } from 'react-bootstrap';



const TaskTable = () => {
  const [tasks, setTasks] = useState([]);
  const [editingTask, setEditingTask] = useState(null); // to hold the task being edited
  const token = localStorage.getItem('token');
  const { taskId } = useParams();

  useEffect(() => {
    fetch('https://letigio-taskapp.onrender.com/task/allTask', {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      }
    })
      .then(response => response.json())
      .then(data => {
        if (Array.isArray(data)) {
          setTasks(data); // Only update state if the received data is an array
        } else {
          console.error('Received data is not an array:', data);
        }
      })
      .catch(error => console.error('Error fetching tasks:', error));
  }, [token, editingTask]);


  const handleEdit = (taskId, updatedTaskData) => {

    fetch(`https://letigio-taskapp.onrender.com/task/${taskId}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      },
      body: JSON.stringify(updatedTaskData)
    })
      .then(response => response.json())
      .then(data => {
        if (data === true) {
          console.log(`Task with ID ${taskId} updated successfully.`);
          Swal.fire({
            icon: 'success',
            title: 'Success',
            text: 'Task updated successfully!'
          });
          setEditingTask(null); // Clear the editing task after successful edit
          // Perform actions after successful update, such as refreshing task data
          // You may want to refetch all tasks to ensure the table is updated
        } else {
          console.log(`Failed to update task with ID ${taskId}.`);
          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'Task update failed!'
          });
          // Handle the scenario where the update fails
        }
      })
      .catch(error => console.error('Error updating task:', error));
  };

  const handleComplete = (taskId) => {
    fetch(`https://letigio-taskapp.onrender.com/task/${taskId}/complete`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      }
    })
      .then(response => response.json())
      .then(data => {
        if (data === true) {
          console.log('Task marked as complete!');
          Swal.fire({
            icon: 'success',
            title: 'Success',
            text: 'Task marked as complete!',
            backdrop: `rgba(0,123,255,0.4)`
          });
          const updatedTasks = tasks.map(task => {
            if (task._id === taskId) {
              return { ...task, completed: true };
            }
            return task;
          });
          setTasks(updatedTasks);
        } else {
          console.log('Task completion failed.');
          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'Task completion failed!'
          });
        }
      })
      .catch(error => console.error('Error marking task as complete:', error));
  };

  const handleDelete = (taskId) => {
    fetch(`https://letigio-taskapp.onrender.com/task/${taskId}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      }
    })
      .then(response => response.json())
      .then(data => {
        if (data === true) {
          console.log('Task deleted!');
          setTasks(tasks.filter(task => task._id !== taskId));
          Swal.fire({
            icon: 'success',
            title: 'Success',
            text: 'Task deleted successfully!'
          });
        } else {
          console.log('Task deletion failed.');
          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'Task deletion failed!'
          });
        }
      })
      .catch(error => console.error('Error deleting task:', error));
  };

  const handleSubmitEdit = (taskId) => {
    if (!editingTask) {
      return;
    }

    handleEdit(taskId, editingTask);
  };

  const handleEditChange = (key, value) => {
    setEditingTask({
      ...editingTask,
      [key]: value
    });
  };

  return (
    <div>
      {tasks.length > 0 && (
      <table>
        <thead>
          <tr>
            <th className="text-center">Title</th>
            <th className="text-center">Description</th>
            <th className="text-center">Priority</th>
            <th className="text-center">Due Date</th>
            <th className="text-center">Actions</th>
          </tr>
        </thead>
        <tbody>
          {tasks.map(task => (
            <tr key={task._id} style={{ border: '1px solid #ccc', borderRadius: '5px', backgroundColor: task.completed ? 'lightgreen' : 'inherit', marginBottom: '5px' }} className="text-center">
              <td className="p-3">
                {editingTask?._id === task._id ? (
                  <input
                    type="text"
                    value={editingTask.title}
                    onChange={(e) => handleEditChange('title', e.target.value)}
                  />
                ) : (
                  task.title
                )}
              </td>
              <td className="p-3">
                {editingTask?._id === task._id ? (
                  <input
                    type="text"
                    value={editingTask.description}
                    onChange={(e) => handleEditChange('description', e.target.value)}
                  />
                ) : (
                  task.description
                )}
              </td>
              <td className="p-3">
                {editingTask?._id === task._id ? (
                  <select
                    value={editingTask.priority}
                    onChange={(e) => handleEditChange('priority', e.target.value)}
                  >
                    <option value="Low">Low</option>
                    <option value="Medium">Medium</option>
                    <option value="High">High</option>
                  </select>
                ) : (
                  task.priority
                )}
              </td>
              <td className="p-3">
                {editingTask?._id === task._id ? (
                  <input
                    type="date"
                    value={editingTask.dueDate}
                    onChange={(e) => handleEditChange('dueDate', e.target.value)}
                  />
                ) : (
                  task.dueDate
                )}
              </td>
              <td className="p-2">
                <div className="d-grid gap-2 my-2 mx-2">
                  {editingTask?._id === task._id ? (
                    <button onClick={() => handleSubmitEdit(task._id)}>Save</button>
                  ) : (
                    <button onClick={() => setEditingTask(task)}>Edit</button>
                  )}
                  <button className="complete" onClick={() => handleComplete(task._id)}>Complete</button>
                  <button className="delete" onClick={() => handleDelete(task._id)}>Delete</button>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      )}
    </div>
  );
};

export default TaskTable;
