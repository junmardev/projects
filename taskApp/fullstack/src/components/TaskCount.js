import React, { useEffect, useState, useContext } from 'react';
import UserContext from '../UserContext';

const TaskCount = () => {
  const { user } = useContext(UserContext);
  const [taskData, setTaskData] = useState({
    completedTasks: 0,
    uncompletedTasks: 0,
    totalTasks: 0
  });

  const fetchTaskSummary = () => {
    fetch('https://letigio-taskapp.onrender.com/task/task-summary', {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then(res => res.json())
      .then(data => {
        setTaskData(data);
      })
      .catch(error => {
        console.error('Error fetching task summary:', error);
      });
  };

  useEffect(() => {
    fetchTaskSummary(); // Initial fetch when the component mounts

    // Fetch task summary periodically every 3 seconds
    const intervalId = setInterval(fetchTaskSummary, 3000);

    // Cleanup the interval on component unmount to prevent memory leaks
    return () => clearInterval(intervalId);
  }, []);

  return (
    <div>
      <p>Completed Task: {taskData.completedTasks}</p>
      <p>Uncompleted Task: {taskData.uncompletedTasks}</p>
      <p>Total Task: {taskData.totalTasks}</p>
    </div>
  );
};

export default TaskCount;
