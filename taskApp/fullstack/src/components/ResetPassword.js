import React, { useState } from 'react';
import Swal from 'sweetalert2';
import { Row, Form, Button } from 'react-bootstrap';
import '../App.css';


const ResetPassword = () => {
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [message, setMessage] = useState('');

  const handleResetPassword = async (e) => {
    e.preventDefault();

    if (password !== confirmPassword) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Passwords do not match',
      });
      return;
    }

    try {
      const token = localStorage.getItem('token');
      const response = await fetch('https://letigio-taskapp.onrender.com/users/reset-password', {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({ newPass: password }),
      });

      if (response.ok) {
        Swal.fire({
          icon: 'success',
          title: 'Password Changed',
          text: 'Password reset successfully',
        });
        setPassword('');
        setConfirmPassword('');
      } else {
        const errorData = await response.json();
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: errorData.message,
        });
      }
    } catch (error) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'An error occurred. Please try again.',
      });
      console.error(error);
    }
  };

  return (
    <div>
      <Form onSubmit={handleResetPassword}>
        <Row>
          <Form.Label htmlFor="password" className="my-1" sm={12}>
            New Password
          </Form.Label>
          <Form.Control
            type="password"
            id="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            required
            sm={12}
          />

          <Form.Label htmlFor="confirmPassword" className="my-1" sm={12}>
            Confirm Password
          </Form.Label>
          <Form.Control
            type="password"
            id="confirmPassword"
            value={confirmPassword}
            onChange={(e) => setConfirmPassword(e.target.value)}
            required
            sm={12}
          />
        </Row>
        {message && <div>{message}</div>}
        <Button type="submit" className="my-3 btn-light">
          Reset Password
        </Button>
      </Form>
    </div>
  );
};
 
export default ResetPassword;

