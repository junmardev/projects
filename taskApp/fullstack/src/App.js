import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes, Navigate, useNavigate } from 'react-router-dom';
import { Container } from 'react-bootstrap';
import { UserProvider } from './UserContext';
import { useState, useEffect } from 'react';
import AppNavbar from './components/AppNavbar';
import TwoColumnLayout from './pages/TwoColumnLayout';
import Login from './pages/Login';
import Register from './pages/Register';
import Logout from './pages/Logout';
import Error from './pages/Error';



function App() {

  const [user, setUser] = useState({
    id: null
  });

    // Function for clearing localStorage on logout
  const unsetUser = () => {

    localStorage.clear();

  };

   useEffect(() => {

    // console.log(user);
    fetch('https://letigio-taskapp.onrender.com/users/details', {
      headers: {
        Authorization: `Bearer ${ localStorage.getItem('token') }`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      // Set the user states values with the user details upon successful login.
      if (typeof data._id !== "undefined") {

        setUser({
          id: data._id,
        });

      // Else set the user states to the initial values
      } else {

        setUser({
          id: null
        });

      }

    })

    }, []);

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
        <AppNavbar />
        <Container>
            <Routes>
                <Route path="/login" element={<Login />} />
                <Route path="/register" element={<Register />} />
                <Route path="/logout" element={<Logout />} />              
                <Route path="/task" element={<TwoColumnLayout />} />
                {/* Adjust the root path to handle redirection based on login status */}
                <Route
                  path="/"
                  element={
                    user.id ? (
                      // If user is logged in, redirect to '/task'
                      <Navigate to="/task" replace />
                    ) : (
                      // If user is not logged in, redirect to '/login'
                      <Navigate to="/login" replace />
                    )
                  }
                />
                <Route path="*" element={<Error />} />
            </Routes>
        </Container>
    </Router>
    </UserProvider>
  );
}

export default App;
