import {Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import {Navigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import { Link, useParams } from 'react-router-dom';
import '../App.css';


export default function Login() {

	const {user, setUser} = useContext(UserContext);

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [isActive, setIsActive] = useState(false);


	function loginUser(event){

		//Prevents the default behavior during submission which is page redirection via form submission
		event.preventDefault();

		const userData = {
			email: email,
			password: password
		}

		fetch('https://letigio-taskapp.onrender.com/users/login', {
			method: "POST",
			headers: {
				"Content-Type" : "application/json"
			},
			body: JSON.stringify(userData)
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			//data is the response from the web service
			//checks the value of response
			console.log(data);

			//if the receive response is true states will be empty and the user will receive a message "Thank you for registering"
			//else, if not receive response is true, the user will receive a messasge "Please try again later"
			if(typeof data.access !== "undefined"){
				localStorage.setItem('token', data.access);

				//function for retrieving user details
				retrieveUserDetails(data.access);

				Swal.fire({
					title: "Login Successful!",
					icon: "success",
					text: "Welcome to Task Management App!"
				})

				// setUser({
				// 	access: localStorage.getItem('token')
				// })

				// alert('Thank you logging in');
			}else {
				Swal.fire({
					title: "Authentication Failed!",
					icon: "error",
					text: "Check your login details and try again!"
				})
			}

		})
		setEmail('');
		setPassword('');
	}

	const retrieveUserDetails = (token) =>{
		fetch('https://letigio-taskapp.onrender.com/users/details', {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setUser({
				id: data._id
			})
		})
	}


	useEffect(() => {
	  if (email !== "" && password !== "") {
	    setIsActive(true);
	  } else {
	    setIsActive(false);
	  }
	}, [email, password]);

	return (
		(user.id !== null)?
		<Navigate to="/task" />
		:
		//else it will use the form login
		<Form onSubmit={(event) => loginUser(event)} className="border p-4 rounded mt-5 mb-5" style={{ maxWidth: '50%', margin: '0 auto' }}>
		<h1 className="my-5 text-center">Login</h1>
			<Form.Group>
				<Form.Label>Email:</Form.Label>
				<Form.Control type="email" placeholder="Enter Email" required value={email} onChange={e => {setEmail(e.target.value)}} />
			</Form.Group>

			<Form.Group className="mt-2">
				<Form.Label>Password:</Form.Label>
				<Form.Control type="password" placeholder="Enter Password" required value={password} onChange={e => {setPassword(e.target.value)}} />
			</Form.Group>
			<p className="mt-3">Don't have an account yet? <Link to="/register">Sign up</Link> here.</p>

			{
				isActive ?
				<Button type="submit" className="mt-3 login-bottom">Login</Button>
				: 
				<Button type="submit" disabled className="mt-3 login-bottom">Login</Button>
			}
		</Form>
	)
}