import React, { useRef, useEffect } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import '../App.css';
import UserDetails from '../components/UserDetails';
import TaskCount from '../components/TaskCount';
import ResetPassword from '../components/ResetPassword';
import AddTaskComponent from '../components/AddTaskComponent';
import TaskTable from '../components/TaskTable';

const TwoColumnLayout = () => {

  const scrollRef = useRef(null);

  const handleScroll = (event) => {
    if (scrollRef.current) {
      const container = scrollRef.current;
      container.scrollLeft += event.deltaY;
    }
  };

  return (
    <Container fluid className="px-0 mx-0">
      <Row className="mt-3">
        {/* Left Column - Takes 4 columns on lg, 6 columns on sm and md */}
        <Col lg={4} sm={6} md={4} className="col-height">
          <div className="left-col p-3 text-light">
              <h6 className="my-2">PROFILE: </h6>
              <div className="horizontal-line">
                <UserDetails />
              </div>
              <h6 className="my-2">TASK: </h6>
              <div className="horizontal-line">
                <TaskCount />
              </div>
              <h6 className="my-2">PASSWORD RESET: </h6>
              <div>
                <ResetPassword />
              </div>
          </div>
        </Col>

        {/* Right Column - Takes 8 columns on lg, 6 columns on sm and md */}
        <Col lg={8} sm={6} md={8} className="col-height">
          <div className="p-3 w-100">
            <h6>Creating/Adding A Task: </h6>
            <div>
              <AddTaskComponent />
            </div>
            <div className="mt-4 pb-3">
              <TaskTable />
            </div>
          </div>
        </Col>
      </Row>
    </Container>
  );
};

export default TwoColumnLayout;
