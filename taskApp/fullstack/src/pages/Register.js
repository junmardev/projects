import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from "../UserContext";
import { Navigate } from 'react-router-dom';
import { Link, useParams } from 'react-router-dom';

export default function Register(){

	const { user } = useContext(UserContext);

	//State hooks to store values of the input fields
	//Syntax: [stateVariable, setterFunction] = useState(assigned state/initial state);
	//Note: setterFunction is to update the state value
	const [name, setName] = useState("");
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");
	const [isActive, setIsActive] = useState(false);

	//Checks if values are successfully binded
	console.log(name);
	console.log(email);
	console.log(password);
	console.log(confirmPassword);

	function registerUser(event) {
		//Prevents the default behavior during submission which is page redirection via form submission
		event.preventDefault();

		fetch('https://letigio-taskapp.onrender.com/users/register', {
			method: "POST",
			headers: {
				"Content-Type" : "application/json"
			},
			body: JSON.stringify({
				name: name,
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			//data is the response from the web service
			//checks the value of response
			console.log(data);

			//if the receive response is true states will be empty and the user will receive a message "Thank you for registering"
			//else, if not receive response is true, the user will receive a messasge "Please try again later"
			if(data){
				setName('');
				setEmail('');
				setPassword('');
				setConfirmPassword('');

				alert('Thank you for registering!');
			}else {
				alert('Please try again later.');
			}

		})		
	}

	//The useEffect will run everytime the state changes in any field oor values listed in the [firstName, lastName, email, mobileNo, password, confirmPassword]
	useEffect(() => {

		if((name !== "" && email !== "" && password !== "" & confirmPassword !== "") && (password === confirmPassword)){
			//sets the isActive value to true to enable the button
			setIsActive(true);
		}else {
			setIsActive(false);
		}

	},[name, email, password, confirmPassword]);


	return(

		(user.id !== null) ?
		<Navigate to='/task' />
		:
		<Form onSubmit={(event) => registerUser(event)} className="border p-4 rounded mt-5 mb-5" style={{ maxWidth: '50%', margin: '0 auto' }}>
		<h1 className="my-5 text-center">Register</h1>
			<Form.Group>
				<Form.Label>Name: </Form.Label>
				<Form.Control type="text" placeholder="Enter Your Name" required value={name} onChange={e => {setName(e.target.value)}} />
			</Form.Group>
			<Form.Group>
				<Form.Label className="mt-2">Email: </Form.Label>
				<Form.Control type="email" placeholder="Enter Email" required value={email} onChange={e => {setEmail(e.target.value)}} />
			</Form.Group>
			<Form.Group>
				<Form.Label className="mt-2">Password: </Form.Label>
				<Form.Control type="password" placeholder="Enter Password" required value={password} onChange={e => {setPassword(e.target.value)}} />
			</Form.Group>
			<Form.Group>
				<Form.Label className="mt-2">Confirm Password: </Form.Label>
				<Form.Control type="password" placeholder="Confirm Password" required value={confirmPassword} onChange={e => {setConfirmPassword(e.target.value)}} />
			</Form.Group>
			<p className="mt-3">Already have an account? <Link to="/login">Sign in</Link> here.</p>

			{
				isActive ?
				<Button type="submit" className="mt-3 login-bottom">Submit</Button>
				: 
				<Button type="submit" disabled className="mt-3 login-bottom">Submit</Button>
			}
			
		</Form>

	);
};