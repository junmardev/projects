import { Button, Row, Col } from 'react-bootstrap';
import {Link, NavLink} from 'react-router-dom';
import '../App.css';

export default function Error(){
	const sampledata = {
	    title: "404 - Not found",
	    content: "The page you are looking for cannot be found",
	    destination: "/",
	    label: "Back Home"
	}

	const { title, content, destination, label } = sampledata;

	return(
		<Row>
			<Col className="p-5 text-center">
				<h1>{title}</h1>
				<p>{content}</p>
				<Link className="btn login-bottom" to={destination}>{label}</Link>
			</Col>
		</Row>
	);
}