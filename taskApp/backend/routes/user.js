 const express = require("express");
const userController = require("../controllers/user");
const auth = require("../auth.js")

// Destructure from auth
const { verify } = auth;

// Routing Component
const router = express.Router();

//User Registration
router.post("/register", userController.registerUser);

//Check if email already Exist
router.post("/checkEmail", userController.checkEmailExist);

//User authentication
router.post("/login", userController.loginUser);

//User Reset Password
router.put("/reset-password", verify, userController.resetPassword);

//View User name and email
router.get("/view/:userId", verify, userController.getUserDetails);

//Get user details
router.get("/details", verify, userController.getProfile);

//Export Route system
module.exports = router;