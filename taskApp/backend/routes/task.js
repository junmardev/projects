const express = require("express");
const taskController = require("../controllers/task");
const auth = require("../auth.js");

// Destructure from auth
const { verify } = auth;

// Routing Component
const router = express.Router();


//Add or creating a new task
router.post("/add-task", verify, taskController.addTask);

//Update or edit task information
router.put("/:taskId", verify, taskController.updateTask);

//Get all tasks created per user
router.get("/allTask", verify, taskController.getAllTask);

// Delete a task by task ID
router.delete("/:taskId", verify, taskController.deleteTask);

//Task completed set to true
router.put("/:taskId/complete", verify, taskController.taskComplete);

// Get the count of completed, uncompleted, and total tasks for the logged-in user
router.get("/task-summary", verify, taskController.getUserTaskSummary);

module.exports = router;