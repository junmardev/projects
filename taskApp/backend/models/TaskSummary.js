const mongoose = require("mongoose");

const taskSummarySchema = new mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    unique: true,
  },
  totalTasks: {
    type: Number,
    default: 0,
  },
  completedTasks: {
    type: Number,
    default: 0,
  },
  uncompletedTasks: {
    type: Number,
    default: 0,
  }
});

module.exports = mongoose.model("TaskSummary", taskSummarySchema);
