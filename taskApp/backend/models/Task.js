const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema({
	title: {
		type: String,
		required: [true, "Title is required!"]
	},
	description: {
		type: String,
		required: [true, "Task description is required!"]
	},
	dueDate: {
        type: Date,
        required: [true, "Due Date is required!"]
    },
    priority: {
        type: String,
        required: [true, "Priority is required!"]
    },
    completed: {
        type: Boolean,
        default: false
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    }
});

module.exports = mongoose.model("Task", taskSchema);
