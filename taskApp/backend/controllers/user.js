//Dependencies and Modules
const User = require("../models/User");
const Product = require("../models/Task");
const bcrypt = require("bcrypt");
const auth = require("../auth");


//User Registration
module.exports.registerUser = (req, res) => {
	let newUser = new User ({
		name: req.body.name,
		email: req.body.email,
		password: bcrypt.hashSync(req.body.password, 10)
	})

	return newUser.save().then((user, error) => {
		if(error){
			return res.send(false)
		} else {
			return res.send(true)
		}
	})
	.catch(err => err);
}

//Check if email already Exist
module.exports.checkEmailExist = (req, res) => {
	return User.find({ email: req.body.email }).then(result => {

		if(result.length > 0){
			return res.send(true)
		} else {
			return res.send(false)
		}
	})
}

//User authentication
module.exports.loginUser = (req, res) => {
	return User.findOne({ email: req.body.email }).then(result => {
		console.log(result);
		if(result === null){
			return res.send(false); // the email doesn't exist in our DB
		} else {

			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

			console.log(isPasswordCorrect);

			if(isPasswordCorrect){
				return res.send({ access: auth.createAccessToken(result) })
			} else {
				return res.send(false); // Password do not match
			}
		}
	})
	.catch(err => res.send(err));
}

//User Reset Password
module.exports.resetPassword = async (req, res) => {
	try{
		const newPass = req.body.newPass;
		const userId = req.user.id;

		// Hashing the new password
		const hashedPassword = await bcrypt.hash(newPass, 10);

		await User.findByIdAndUpdate(userId, {password: hashedPassword});

		res.status(200).json({message: true});
	}catch(error) {
		res.status(500).json({message: false});
	}
}

//View User name and email
module.exports.getUserDetails = (req, res) => {
  return User.findById(req.params.userId).then(user => {
    if (!user) {
      return res.status(404).json({});
    } else {
      return res.send({ name: user.name, email: user.email });
    }
  }).catch(err => {
    return res.status(500).json({ message: "Error retrieving user details" });
  });
};

//Get user details
module.exports.getProfile = (req, res) => {
	return User.findById(req.user.id).then(result => {
		result.password = "";
		return res.send(result);
	})
	.catch(error => error)
}

