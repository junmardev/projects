//Dependencies and Modules
const User = require("../models/User");
const Task = require("../models/Task");
const TaskSummary = require("../models/TaskSummary");
const bcrypt = require("bcrypt");
const auth = require("../auth");


//Add or creating a new task
module.exports.addTask = (req, res) => {
	const userId = req.user.id;

	let newTask = new Task({
		user: userId,
		title: req.body.title,
		description: req.body.description,
		dueDate: req.body.dueDate,
		priority: req.body.priority
	})

	return newTask.save().then((task, error) => {
		if(error) {
			return res.send(false)
		}else {
			return res.send(true)
		}
	})
	.catch(err => res.send(err))
}

//Update or edit task information
module.exports.updateTask = (req, res) => {
	const userId = req.user.id;

	let updateATask = {
		user: userId,
		title: req.body.title,
		description: req.body.description,
		dueDate: req.body.dueDate,
		priority: req.body.priority
	}

	return Task.findByIdAndUpdate(req.params.taskId, updateATask).then((task, error) => {
		if(error) {
			return res.send(false);
		}else {
			return res.send(true);
		}
	})
}

//Get all tasks created per user
module.exports.getAllTask = (req, res) => {
	return Task.find({ user: req.user.id }).then(result => {
		if(result.length === 0){
			return res.send(false);
		}else {
			return res.send(result);
		}
	})
}

// Delete a task by task ID
module.exports.deleteTask = (req, res) => {
  const userId = req.user.id;
  const taskId = req.params.taskId;

  // Find and remove the task by its ID and user
  Task.findOneAndRemove({ _id: taskId, user: userId })
    .then(task => {
      if (!task) {
        return res.send(false); // Task not found, return false
      }
      return res.send(true); // Task deleted successfully, return true
    })
    .catch(err => {
      return res.status(500).json({ message: "Error deleting task" });
    });
};

//Task completed set to true
module.exports.taskComplete = (req, res) => {
	let taskCompleted = {
		completed: true
	}

	return Task.findByIdAndUpdate(req.params.taskId, taskCompleted).then((product, error) => {
		if(error) {
			return res.send(false);
		}else {
			return res.send(true);
		}
	})
	.catch(error => res.send("Please enter a correct task ID"));
}

// Get the count of completed, uncompleted, and total tasks for the logged-in user
module.exports.getUserTaskSummary = (req, res) => {
  const userId = req.user.id;

  // Count completed tasks for the logged-in user
  const completedTaskCount = Task.countDocuments({ user: userId, completed: true });

  // Count uncompleted tasks for the logged-in user
  const uncompletedTaskCount = Task.countDocuments({ user: userId, completed: false });

  // Count total tasks for the logged-in user
  const totalTaskCount = Task.countDocuments({ user: userId });

  Promise.all([completedTaskCount, uncompletedTaskCount, totalTaskCount])
    .then(([completedCount, uncompletedCount, totalTasksCount]) => {
      res.send({
        completedTasks: completedCount,
        uncompletedTasks: uncompletedCount,
        totalTasks: totalTasksCount
      });
    })
    .catch(err => {
      res.status(500).send("Error retrieving task summary: " + err.message);
    });
};


