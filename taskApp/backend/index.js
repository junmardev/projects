//Dependencies and Modules
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

require('dotenv').config();

// Allows access to routes defined within our application
const userRoutes = require("./routes/user"); 
const taskRoutes = require("./routes/task");

//Environment setup
const port = 4005;

//Server setup
const app = express();

app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Allows all resources to access our backend application
app.use(cors());

//Database Connection
mongoose.connect("mongodb+srv://admin:admin123@cluster0.y71e1rp.mongodb.net/Task-Management-App?retryWrites=true&w=majority", {
	useNewUrlParser : true,
	useUnifiedTopology : true
});

mongoose.connection.once('open', () => console.log("The Server is now connected to MongoDB Atlas!"));


// Backend Routes
app.use("/users", userRoutes);
app.use("/task", taskRoutes);


//Server Gateway Response
if(require.main === module){
	app.listen(process.env.PORT || port, () => {
		console.log(`API is now online on port ${process.env.PORT || port}`)
	});
};

module.exports = app;