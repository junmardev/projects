//[SECTION] Variables ----------------------------------------->>>

//variable let
let variable1 = 3;
console.log(variable1);

let calculation = 2 + 2; 
console.log(calculation);
console.log(calculation + 2);

let result = calculation + 2;
console.log(result);

let message = 'hello';
console.log(message);

//re-assigning a variable
variable1 = 5;
console.log(variable1);

variable1 = variable1 + 1;
console.log(variable1);

//const or constant, value can't be change and will stay constant
const variable2 = 3;


//var, is the original way to create variable used in older JavaScript code
var variable3 = 3;

//typeof is used to display the data type value
console.log(typeof 3);
console.log(typeof variable2);
console.log(typeof message);

