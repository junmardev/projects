//creating an alert

/*

window.alert("I love pizza");

*/

//[SECTION] variable ----------------------------------------->>>>>>>>>>>
/*

A variable is a container for storing data
A variable behaves as if it was the value that it contains

two steps: 
1. Declaration (var, let, const)
2. Assignment (= assignment operator)

*/

//variable declaration and assignment

/*
let firstName = "Ramnuj"; //string
let age = 21; //numbers
let student = false; //boolean


console.log("Hello", firstName);
console.log("You are", age, "years old");
console.log("Enrolled:", student);

*/
//[SECTION] DOM manipulation ----------------------------------------->>>>>>>>>>>

//DOM Manipulation
/*
document.getElementById("1").innerHTML = "Hello" + firstName;
document.getElementById("2").innerHTML = "You are" + age + "years old";
document.getElementById("3").innerHTML = "Enrolled: " + student;
*/


//[SECTION] Arithmetic expression ----------------------------------------->>>>>>>>>>>

/*
arithmetic expression is a combination of..
operands (values, variables, etc.)
operators (+ - * / %)
that can be evaluated to a value
example: y = x + 5; 

*/

let studentA = 20;

//studentA = studentA + 2;
//studentA = studentA - 2;
//studentA = studentA * 2;
// studentA = studentA / 2;
//augmented -> a short cut 
// studentA += 1;

/*

//modulo
let extraStudents = studentA % 3; // answer 2 -> since 20 / 3 has remainder 2 

*/

console.log(studentA);
// console.log(extraStudents);


/*

operator precedence
1. parenthesis ()
2. exponents
3. multiplication & division
4. addition and subtraction

also known as PEMDAS

*/

//based on operator precedence should be from read from right to left
// first parenthesis () | 3 + 4 = 7 |  7 * 2 = 14 | then finally 14 + 1 = 15 | hence result is 15
let result = 1 + 2 * (3 + 4);

//you can force the result to 21 by inserting 1 + 2 inside a parenthesis
//example: let result = (1 + 2) * (3 + 4); -> result is 21

console.log("result from -> let result:", result);


//[SECTION] Accepting User Input ----------------------------------------->>>>>>>>>>>

// EASY WAY with a window prompt
/*

let username = window.prompt("What's your name?");
console.log(username);

*/

//DIFFICULT WAY HTML textbox
/*
let username;
document.getElementById("myButton").onclick = function() {

	username = document.getElementById("myText").value;
	console.log(username);
	document.getElementById("myLabel").innerHTML = "Hello, " + username;
}


*/


//[SECTION] Type Conversion ----------------------------------------->>>>>>>>>>>

// change the datatype of a value to another (string, numbers or booleans)

/*

let age = window.prompt("How old are you?");

age = Number(age); //result if we type 27 is 28, since we're converting the string to a number using Number()
age += 1;

console.log("Happy Birthday! You are ", age, "years old"); // the result would be 271 if I input 27 since age is currently a string were only concanating it. To convert string we have to use -> age = Number(age);

*/

//another example: 

/*

let x;
let y;
let z;

x = Number("3.14");
y = String(3.14);
z = Boolean("pizza"); // if we pass an empty string it will give a result false else if we pass any string it will return true

console.log(x, typeof x);
console.log(y, typeof y);
console.log(z, typeof z);

*/

//[SECTION] CONST or Constant ----------------------------------------->>>>>>>>>>>

// a variable that can't be changed, it adds a little bit security
/*
const PI = 3.14159;
let radius;
let circumference;

radius = window.prompt("Enter the radius of a circle");
radius = Number(radius);

circumference = 2 * PI * radius;

console.log("The circumference is: ", circumference);

*/

//[SECTION] MATH ----------------------------------------->>>>>>>>>>>

let x = 3.14;

//round is used to round a number to nearest integer. If the decimal part of the number is 0.5 or greater, the number is rounded up; otherwise, it is rounded down.
// x = Math.round(x); 

// always rounds a number down to the nearest integer that is less than or equal to the original number. It essentially removes the decimal part of the number and gives you the largest integer that is less than or equal to the original number.
// x = Math.floor(x); 

// rounds a number up to the nearest integer. It essentially moves towards positive infinity, giving you the smallest integer that is greater than or equal to the original number.
// x = Math.ceil(x);

//When you use Math.pow(x, 2), you are squaring the value of x. The computation is equivalent to multiplying x by itself. so Math.pow(x, 2) = 3.99 × 3.99 = 15.9201
// x = Math.pow(x, 2);

//Math.sqrt() is used to calculate the square root of a number.
// x = Math.sqrt(x);

//The Math.abs() function in JavaScript is used to return the absolute value of a number. The absolute value of a number is its distance from zero on the number line, regardless of direction. In other words, it gives you the non-negative value of a number.
// x = Math.abs(x);

console.log(x);

/*

let y = 5;
let z = 9;
let maximum;
let minimum;

maximum = Math.max(x, y, z); // maximum is 9
minimum = Math.min(x, y, z); // minimum is 3.14


let x;
x = Math.PI;
console.log(x); -> result is 3.141592653589793

console.log(maximum);
console.log(minimum);

*/

// hypotenuse calc practice program
let a;
let b;
let c;

/*

a = window.prompt("Enter side A");
a = Number(a);

b = window.prompt("Enter side B");
b = Number(b);

c = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));

console.log("Side C: ", c);

*/

document.getElementById("submitButton").onclick = function() {

	a = document.getElementById("aTextBox").value;
	a = Number(a);

	b = document.getElementById("bTextBox").value;
	b = Number(b);

	c = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));

	document.getElementById("cLabel").innerHTML = "Side C: " + c;

}