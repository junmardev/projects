//grap = nodes + edges
//nodes circle or data 
//edge is connection of the pair of nodes

//Depth first: Stack
//Breadth first: Queue

//Depth first traversal sample

/*

const depthFirstPrint = (graph, source) => {
	const stack = [ source ];

	while(stack.length > 0) {
		const current = stack.pop();
		console.log(current);
		for (let neighbor of graph[current]) {
			stack.push(neighbor);
		}
	}
};

*/


//another example of depthFirst
/*

const depthFirstPrint = (graph, source) => {
	console.log(source);
	for(let neighbor of graph[source]){
		depthFirstPrint(graph, neighbor);
	}
}; //works same as the first arrow function code

*/

//sample of breadthFirst
const breadthFirstPrint = (graph, source) => {
	const queue = [source];
	while(queue.length > 0) {
		const current = queue.shift();
		console.log(current);
		for(let neighbor of graph[current]) {
			queue.push(neighbor);
		}
	}
}; //output will be acbedf 


const graph = {
	a: ['c', 'b'], //if changed b, c result is acebdf otherwise if c, b result is abdfce
	b: ['d'],
	c: ['e'],
	d: ['f'],
	e: [],
	f: []
}

// depthFirstPrint(graph, 'a');

breadthFirstPrint(graph, 'a');




































