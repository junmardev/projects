import React from 'react';
import { Navbar, Container, Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import '../App.css';

const AppNavbar = () => {
  return (
    <Navbar bg="black" variant="black" expand="lg" className="navbar-style">
      <Container>
        <Navbar.Brand as={Link} to="/" className="navbar-font universal-color">Junmar<span className="specific-letter">Dev.</span></Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" className="navbar-humburger" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto">
          	<Nav.Link as={Link} to="/" className="navbar-font universal-color navbar-hover">HOME</Nav.Link>
            <Nav.Link as={Link} to="/about" className="navbar-font universal-color navbar-hover">ABOUT</Nav.Link>
            <Nav.Link as={Link} to="/projects" className="navbar-font universal-color navbar-hover">PROJECTS</Nav.Link>
            <Nav.Link as={Link} to="/contact" className="navbar-font universal-color navbar-hover">CONTACT</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default AppNavbar;
