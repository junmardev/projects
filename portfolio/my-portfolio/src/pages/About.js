import React, { useState } from 'react';
import '../App.css';

const About = () => {
    const [activeTab, setActiveTab] = useState('about-me');

    const handleTabChange = (tab) => {
      setActiveTab(tab);
    };

    const AboutMe = () => {
      return (
        <div className="text-center pt-5 text-justify about-section">
          <p className="about-txt">Hey there, I'm Junmar Letigio. Over the years, I've been devoted to customer service in the BPO industry and as a freelancer, which has provided me financial stability. But my true passion? It's always been web development and design. To pursue that, I've invested time and resources into online coding courses, both free and paid. Recently, I completed an intensive coding training bootcamp at Zuitt. Now, my goal is to land a position where I can merge my passion with my work.</p>
          <p className="about-txt">Moreover, I'm eager and motivated to keep learning and embrace the newest technologies in web development and software engineering. I'm a quick learner, and I've got a strong ability to adapt to new concepts and technologies.</p>
        </div>
      );
    };

    const Experience = () => {
      return (
        <div className="pt-5 text-justify about-section">
          <p className="about-txt"><span className="specific-letter">>_</span> Designed and developed a static responsive coffee shop website using HTML5, CSS3, Bootstrap, and Git for version control, hosted on GitHub.</p>
          <p className="about-txt"><span className="specific-letter">>_</span> Created an e-commerce backend using Node, Express.js, and MongoDB, with user authentication, checkout, and order history. Admin features include product management, editing, archiving, and adding new products.</p>
          <p className="about-txt"><span className="specific-letter">>_</span> Developed a Full-Stack E-Commerce Web Application, implementing CRUD functionality and integrating APIs using the MERN stack.</p>
          <p className="about-txt"><span className="specific-letter">>_</span> Created a task management backend API using Node.js, Express.js, and MongoDB, featuring user authentication. It enables users to add tasks with detailed information like title, description, priority, and due dates, as well as edit, complete, and delete tasks.</p>
          <p className="about-txt"><span className="specific-letter">>_</span> Additionally, I've designed a comprehensive Full Stack Task Management Web Application that embodies seamless CRUD functionality. This application is built using the MERN stack.</p>
        </div>
      );
    };

    const Education = () => {
      return (
        <div className="text-center pt-5 about-section">
          <h5>Zuitt Coding Bootcamp:</h5>
          <p className="about-educ">Duration: July 2023 - October 2023</p>
          <p className="about-educ">Received comprehensive training in the MERN stack, including the use of Postman for API and backend testing.</p>
          <h5 className="mt-5">Cebu Technological University - Main Campus:</h5>
          <p className="about-educ">Duration: 2014-2019</p>
          <p className="about-educ">Bachelor of Science in Industrial Technology - Major in Computer Technology</p>
        </div>
      );
    };

    const renderTabContent = () => {
      switch (activeTab) {
        case 'about-me':
          return <AboutMe />;
        case 'experience':
          return <Experience />;
        case 'education':
          return <Education />;
        default:
          return <AboutMe />;
      }
    };

    return (
      <div className="universal-color">
        <ul className="about-tabs">
          <li onClick={() => handleTabChange('about-me')} className="about-tab">Introduction</li>
          <li onClick={() => handleTabChange('experience')} className="about-tab">Experience</li>
          <li onClick={() => handleTabChange('education')} className="about-tab">Education</li>
        </ul>

        {renderTabContent()}
      </div>
    );
};

export default About;