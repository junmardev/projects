import React, { useRef } from 'react';
import emailjs from '@emailjs/browser';
import '../App.css';
import { Form, FormGroup, Button } from 'react-bootstrap';


const Contact = () => {
  const form = useRef();

  const sendEmail = (e) => {
    e.preventDefault();

    emailjs.sendForm('service_igdimli', 'template_qj18k6h', form.current, 'DC6OXgAMi7jzY1Cig')
      .then((result) => {
          alert("Your message has been sent successfully! \uD83D\uDE00");
      }, (error) => {
          alert("Unable to send message at this time! \u2639\uFE0F");
      });
  };

  return (
    <div className="d-flex justify-content-center align-items-center vh-100">
      <Form ref={form} onSubmit={sendEmail} className="form-styling">
        <FormGroup>
          <Form.Label className="universal-color py-2">Name or Company Name</Form.Label>
          <Form.Control type="text" name="from_name" required />
        </FormGroup>

        <FormGroup>
          <Form.Label className="universal-color py-2">Email</Form.Label>
          <Form.Control type="email" name="from_email" required />
        </FormGroup>

        <FormGroup>
          <Form.Label className="universal-color py-2">Message</Form.Label>
          <Form.Control as="textarea" rows={3} name="message" required />
        </FormGroup>

        <Button type="submit" className="form-button py-2 mt-3">Send Message</Button>
      </Form>
    </div> 
  );
};

export default Contact;