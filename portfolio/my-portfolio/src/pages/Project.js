import Card from 'react-bootstrap/Card';
import { Container, Row, Col } from 'react-bootstrap';
import React from 'react';
import '../App.css';
import img1 from '../images/static.png';
import img2 from '../images/back-ecommerce.png';
import img3 from '../images/full-ecommerce.png';
import img4 from '../images/back-taskapp.png';
import img5 from '../images/full-taskapp.png';


const Project = () => {

  return (
    <Container className="mt-3">

      <Row>
        <Col lg={4} md={6} sm={12} className="mt-5 col-hover">
            <Card style={{ width: '20rem', height: '30rem'}}>
              <Card.Img variant="top" src={img2} />
              <Card.Body className="project-card">
                <Card.Title className="project-card project-txt">Back End E-Commerce API</Card.Title>
                <div className="project-card project-skillcard">Node</div>
                <div className="project-card project-skillcard">Express</div>
                <div className="project-card project-skillcard">Mongo</div>
                <div className="project-card project-skillcard">API</div>
                <Card.Text className="project-card project-txt project-cardtxt">
                  Created an E-Commerce backend using Node, Express.js, and MongoDB, with user authentication, checkout, and order history. Admin features include product management, editing, archiving, and adding new products.
                </Card.Text>
              </Card.Body>
              <Card.Body className="project-card-bottom">
                <Card.Link href="https://gitlab.com/junmardev/sessions/-/tree/master/capstone/final-capstone-3/capstone-2?ref_type=heads" target="_blank" className="project-card project-txt btn btn-primary">Source Code</Card.Link>
                <Card.Link href="https://capstone2-letigio.onrender.com/" target="_blank" className="project-card project-txt btn btn-primary">Open API</Card.Link>
              </Card.Body>
            </Card>
        </Col>

        <Col lg={4} md={6} sm={12} className="mt-5 col-hover">
            <Card style={{ width: '20rem', height: '30rem'}}>
              <Card.Img variant="top" src={img3} />
              <Card.Body className="project-card">
                <Card.Title className="project-card project-txt">Full Stack E-Commerce Web Application</Card.Title>
                <div className="project-card project-skillcard">Node</div>
                <div className="project-card project-skillcard">Express</div>
                <div className="project-card project-skillcard">Mongo</div>
                <div className="project-card project-skillcard">React</div>
                <div className="project-card project-skillcard">API</div>
                <Card.Text className="project-card project-txt project-cardtxt">
                Developed a Full-Stack E-Commerce Web Application, implementing CRUD functionality and integrating APIs using the MERN stack.
                </Card.Text>
              </Card.Body>
              <Card.Body className="project-card-bottom">
                <Card.Link href="https://gitlab.com/junmardev/sessions/-/tree/master/capstone/final-capstone-3/full-stack/src?ref_type=heads" target="_blank" className="project-card project-txt btn btn-primary">Source Code</Card.Link>
                <Card.Link href="https://letigio-fullstack-ecommerce-app-green.vercel.app/" target="_blank" className="project-card project-txt btn btn-primary">Open Website</Card.Link>
              </Card.Body>
            </Card>
        </Col>

        <Col lg={4} md={6} sm={12} className="mt-5 col-hover">
            <Card style={{ width: '20rem', height: '30rem'}}>
              <Card.Img variant="top" src={img4} />
              <Card.Body className="project-card">
                <Card.Title className="project-card project-txt">Back End Task Management API</Card.Title>
                <div className="project-card project-skillcard">Node</div>
                <div className="project-card project-skillcard">Express</div>
                <div className="project-card project-skillcard">Mongo</div>
                <div className="project-card project-skillcard">API</div>
                <Card.Text className="project-card project-txt project-cardtxt">
                  Created a Task Management backend API using Node.js, Express.js, and MongoDB, featuring user authentication. It enables users to add tasks with detailed information like title, description, priority, and due dates, as well as edit, complete, and delete tasks.
                </Card.Text>
              </Card.Body>
              <Card.Body className="project-card-bottom">
                <Card.Link href="https://gitlab.com/junmardev/projects/-/tree/main/taskApp/backend?ref_type=heads" target="_blank" className="project-card project-txt btn btn-primary">Source Code</Card.Link>
                <Card.Link href="https://letigio-taskapp.onrender.com/" target="_blank" className="project-card project-txt btn btn-primary">Open API</Card.Link>
              </Card.Body>
            </Card>
        </Col>


        <Col lg={4} md={6} sm={12} className="mt-5 col-hover">
            <Card style={{ width: '20rem', height: '30rem'}}>
              <Card.Img variant="top" src={img5} />
              <Card.Body className="project-card">
                <Card.Title className="project-card project-txt">Full Stack Task Management Web Application</Card.Title>
                <div className="project-card project-skillcard">Node</div>
                <div className="project-card project-skillcard">Express</div>
                <div className="project-card project-skillcard">Mongo</div>
                <div className="project-card project-skillcard">React</div>
                <div className="project-card project-skillcard">API</div>
                <Card.Text className="project-card project-txt project-cardtxt">
                  Developed a comprehensive Full Stack Task Management Web Application that embodies seamless CRUD functionality. This application is built using the MERN stack.
                </Card.Text>
              </Card.Body>
              <Card.Body className="project-card-bottom">
                <Card.Link href="https://gitlab.com/junmardev/projects/-/tree/main/taskApp/fullstack?ref_type=heads" target="_blank" className="project-card project-txt btn btn-primary">Source Code</Card.Link>
                <Card.Link href="https://letigio-taskmanagement-app.vercel.app/login" target="_blank" className="project-card project-txt btn btn-primary">Open Website</Card.Link>
              </Card.Body>
            </Card>
        </Col>

        <Col lg={4} md={6} sm={12} className="my-5 col-hover">
            <Card style={{ width: '20rem', height: '30rem'}}>
              <Card.Img variant="top" src={img1} />
              <Card.Body className="project-card">
                <Card.Title className="project-card project-txt">Aroma Avenue</Card.Title>
                <div className="project-card project-skillcard">HTML</div>
                <div className="project-card project-skillcard">CSS</div>
                <div className="project-card project-skillcard">Bootstrap</div>
                <div className="project-card project-skillcard">Javascript</div>
                <Card.Text className="project-card project-txt project-cardtxt">
                  Designed and developed a static responsive coffee shop website using HTML5, CSS3, Bootstrap, and Git for version control, hosted on GitHub.
                </Card.Text>
              </Card.Body>
              <Card.Body className="project-card-bottom">
                <Card.Link href="https://github.com/letigiojun/aroma-avenue" target="_blank" className="project-card project-txt btn btn-primary">Source Code</Card.Link>
                <Card.Link href="https://letigiojun.github.io/aroma-avenue/" target="_blank" className="project-card project-txt btn btn-primary">Open Website</Card.Link>
              </Card.Body>
            </Card>
        </Col>

      </Row>

    </Container>
  );
}

export default Project;