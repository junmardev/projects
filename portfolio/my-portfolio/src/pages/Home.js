import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import '../App.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLinkedin, faGithub, faGitlab } from '@fortawesome/free-brands-svg-icons';
import img from '../images/10276612_4421964-removebg-preview.png';
import { useState, useEffect } from 'react';


const Home = () => {

  const linkedinURL = 'https://www.linkedin.com/in/junmar-letigio-a4a602230/';
  const githubURL = 'https://github.com/letigiojun';
  const gitlabURL = 'https://gitlab.com/junmardev';

  const [text, setText] = useState('');
  const sentences = ["I'm Junmar"];
  let count = 0;
  let index = 0;

  useEffect(() => {
    type();
  }, []);

  function type() {
    if (count === sentences.length) {
      count = 0;
    }
    let currentText = sentences[count];
    setText(currentText.slice(0, ++index));

    if (index === currentText.length) {
      index = 0;
      count++;
    }

    setTimeout(type, 500); // Adjust the speed of typing animation by changing the time (in milliseconds)
  }


  return (
    <Container fluid>
      <Row>
        <Col xs={12} md={6} className="mt-2 mx-auto d-flex">
          <div className="intro">
            <h1 className="text-center universal-color typing-animation">Hello, {text}</h1>
            <h2 className="text-center universal-color">A <span className="specific-letter">Full-Stack</span> Web Developer</h2>
            <div className="socials">
              <h3 className="pt-5 universal-color">Get In Touch <span className="specific-letter">>_</span></h3>
              <div className="pt-2">
                <a href={linkedinURL} target="_blank" rel="noopener noreferrer">
                  <FontAwesomeIcon icon={faLinkedin} size="2x" className="icon p-2" />
                </a>
                <a href={githubURL} target="_blank" rel="noopener noreferrer">
                  <FontAwesomeIcon icon={faGithub} size="2x" className="icon p-2" />
                </a>
                <a href={gitlabURL} target="_blank" rel="noopener noreferrer">
                  <FontAwesomeIcon icon={faGitlab} size="2x" className="icon p-2" />
                </a>
              </div>
            </div>
          </div>
        </Col>

        <Col xs={12} md={6} className="mt-2">
          <Row>
            <Col xs={12}>
              <img src={img} alt="home page image" className="img-fluid" />
            </Col>
          </Row>
        </Col>
      </Row>

      <Row>
          <Col xs={12}>
            <div className="skills text-center mx-5 px-5">
              <h4 className="mb-2 universal-color">SKILLS <span className="specific-letter">>_</span></h4>
                <div className="skill-box">HTML</div>
                <div className="skill-box">CSS</div>
                <div className="skill-box">Bootstrap</div>
                <div className="skill-box">Javascript</div>
                <div className="skill-box">React</div>
                <div className="skill-box">Node.JS</div>
                <div className="skill-box">Express.JS</div>
                <div className="skill-box">MongoDB</div>
                <div className="skill-box">Postman</div>
                <div className="skill-box">API</div>
            </div>
          </Col>
      </Row>
    </Container>
  );
};

export default Home;
